/*
    SPDX-FileCopyrightText: 2024 Jin Liu <m.liu.jin@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "kcm_strokognitioneffect.h"

// generated
#include <strokognitionconfig.h>

// KWin
#include <config-kwin.h>
#include <kwineffects_interface.h>

// KDE Frameworks
#include <KLocalizedString>
#include <KPluginFactory>

K_PLUGIN_CLASS(KWin::KCMStrokognitionEffect)

namespace KWin
{

KCMStrokognitionEffect::KCMStrokognitionEffect(QObject *parent, const KPluginMetaData &data)
    : KCModule(parent, data)
{
    StrokognitionConfig::instance(KWIN_CONFIG);
    m_ui.setupUi(widget());

    addConfig(StrokognitionConfig::self(), widget());
}

KCMStrokognitionEffect::~KCMStrokognitionEffect()
{
}

void KCMStrokognitionEffect::load()
{
    KCModule::load();
}

void KCMStrokognitionEffect::save()
{
    KCModule::save();
    OrgKdeKwinEffectsInterface interface(QStringLiteral("org.kde.KWin"), QStringLiteral("/Effects"), QDBusConnection::sessionBus());
    interface.reconfigureEffect(QStringLiteral("strokognition"));
}

void KCMStrokognitionEffect::defaults()
{
    KCModule::defaults();
}

} // namespace

#include "kcm_strokognitioneffect.moc"

#include "moc_kcm_strokognitioneffect.cpp"
