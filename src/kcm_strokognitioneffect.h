/*
    SPDX-FileCopyrightText: 2024 Jin Liu <m.liu.jin@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

// KDE Frameworks
#include <KCModule>

// generated
#include <ui_kcm_strokognitioneffect.h>

namespace KWin
{
class KCMStrokognitionEffect : public KCModule
{
    Q_OBJECT
public:
    explicit KCMStrokognitionEffect(QObject *parent, const KPluginMetaData &data);
    ~KCMStrokognitionEffect() override;

public Q_SLOTS:
    void save() override;
    void load() override;
    void defaults() override;

private:
    Ui::StrokognitionEffectConfigWidget m_ui;
};

} // namespace
