kcoreaddons_add_plugin(strokognition INSTALL_NAMESPACE "kwin/effects/plugins/")
target_sources(strokognition PRIVATE
    strokognitioneffect.cpp
    main.cpp
    stroke.cpp
)
kconfig_add_kcfg_files(strokognition strokognitionconfig.kcfgc)

target_link_libraries(strokognition
    KF6::ConfigCore
    KF6::ConfigGui
    KF6::CoreAddons
    KF6::I18n

    KWin::kwin
)

set(kwin_strokognition_config_SOURCES kcm_strokognitioneffect.cpp)
ki18n_wrap_ui(kwin_strokognition_config_SOURCES kcm_strokognitioneffect.ui)
qt_add_dbus_interface(kwin_strokognition_config_SOURCES ${KWIN_EFFECTS_INTERFACE} kwineffects_interface)

kcoreaddons_add_plugin(kwin_strokognition_config INSTALL_NAMESPACE "kwin/effects/configs" SOURCES ${kwin_strokognition_config_SOURCES})
kconfig_add_kcfg_files(kwin_strokognition_config strokognitionconfig.kcfgc)
target_link_libraries(kwin_strokognition_config
    KF6::ConfigCore
    KF6::ConfigGui
    KF6::ConfigWidgets
    KF6::CoreAddons
    KF6::I18n
    KF6::KCMUtils
    KF6::XmlGui

    KWin::kwin
)
