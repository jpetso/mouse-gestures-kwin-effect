/*
    SPDX-FileCopyrightText: 2024 Jin Liu <m.liu.jin@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include "stroke.h"

// KWin
#include <effect/effect.h>
#include <effect/offscreenquickview.h>
#include <input.h>

// Qt
#include <QMap>
#include <QTimer>
#include <QVariantAnimation>

// std
#include <chrono>
#include <unordered_map>
#include <vector>

namespace KWin
{

class Cursor;
class GLTexture;

class StrokognitionEffect : public Effect, public InputEventFilter
{
    Q_OBJECT

public:
    StrokognitionEffect();
    ~StrokognitionEffect() override;

    static bool supported();

    void reconfigure(ReconfigureFlags flags) override;
    bool pointerEvent(MouseEvent *event, quint32 nativeButton) override;
    bool isActive() const override;
    void paintScreen(const RenderTarget &renderTarget, const RenderViewport &viewport, int mask, const QRegion &region, Output *screen) override;

private:
    bool mouseButtonPressEvent(MouseEvent *event, quint32 nativeButton);
    bool mouseButtonReleaseEvent(MouseEvent *event, quint32 nativeButton);
    bool mouseMoveEvent(MouseEvent *event);

    struct ButtonGrab;
    void endStroke();
    void releaseInactiveButtonGrabs();
    void releaseButtonGrab(InputDevice *device, ButtonGrab &grab);

    void startInAnimation();
    void startOutAnimation();
    void updateMaxScale();

private Q_SLOTS:
    void onInputDeviceDestroyed(QObject *device);

private:
    // configuration
    int m_animationTime;
    int m_spotlightRadius;
    std::chrono::milliseconds m_startButtonlessStrokeTimeout = std::chrono::milliseconds(0);
    std::chrono::milliseconds m_endButtonlessStrokeTimeout = std::chrono::milliseconds(0);

    Qt::MouseButton m_activationButton = Qt::RightButton;

    /**
     * Minimum distance for pointer movement (in KWin::MouseEvent::position() units) at which
     * strokes start getting recognized.
     */
    qreal m_activationDistance = 16.0;

    // matched set of strokes/gestures
    using StrokeId = std::uint32_t;
    std::unordered_map<StrokeId, Stroke> m_availableStrokes;

    // runtime stroke tracking
    struct ButtonGrab {
        std::vector<QPointF> points;
        quint32 nativeButton = 0;
        std::chrono::microseconds lastTimestamp;
        bool releasing = false;
    };
    QMap<InputDevice *, ButtonGrab> m_buttonGrabs;
    InputDevice *m_activeGrabDevice = nullptr;
    QTimer m_buttonlessStrokeTimer;

    // visualization
    QVariantAnimation m_inAnimation;
    QVariantAnimation m_outAnimation;
    qreal m_animationValue; // value of the animation, from 1.0 (largest circle) to 0.0 (smallest circle)
    qreal m_maxScale; // max scale of the strokognition texture, corrsponding to m_animationValue=1.0

    std::unique_ptr<GLTexture> m_strokognitionTexture;
};

} // namespace KWin
