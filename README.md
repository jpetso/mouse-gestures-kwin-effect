# Strokognition

KWin effect (plugin) for recognizing & visualizing pointer strokes a.k.a. mouse gestures.
We'll see how the configuration and action invocation parts play out.
This repository is a prototype, a stable version is meant to get upstreamed into Plasma eventually.

## Requirements

KDE Plasma 6.0 (Wayland session)

## Build & install

Clone the repository, build the library and install it into `/usr/lib/qt6/plugins/`
(or your local dev prefix equivalent):

```bash
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release
cmake --build build
sudo cmake --install build
```

## Thanks

To Jin Liu for creating the Spotlight effect for KWin (https://github.com/jinliu/kwin-effect-spotlight),
which was used as a quick-start template.
